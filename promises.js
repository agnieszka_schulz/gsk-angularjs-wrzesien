function echo(m,err){
	return new Promise(function(resolve, reject){
		setTimeout(function(){
            err? reject(err) : resolve(' '+m)
        },2000)
    })
}
// ===

m1 = echo('Hello')

m1
.then(function(m) { 
	return echo(m + ' World','Upss.. ')
})
.catch( function(e){ return ' World Not Found' })
.then(function(m) { console.log('RESULT',m) })

// ====

m1 = echo('Hello')

m1
.then(function(m) { 
	return echo(m + ' World')
})
.then(function(m) { console.log(m) })