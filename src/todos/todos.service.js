angular.module('todos.module')
  .service('Todos', function ($http) {

    this.fetchTodos = function () {

      return $http.get('http://localhost:3000/todos')
        .then(function (resp) {
          
          return resp.data
        })
    }

  })