angular.module('users.module')
  .service('UsersService', function ($http) {

    var apiUrl = 'http://localhost:3000/users/';

    this.fetchUsers = function () {
      return $http.get(apiUrl)
        .then(function (resp) {
          return resp.data
        })
    }

    this.saveUser = function (user) {
      return $http.put(apiUrl + user.id, user)
        .then(function (resp) {
          return resp.data
        })
    }

    this.deleteUser = function (user) {
      return $http.delete(apiUrl + user.id)
    }

    this.createUser = function (user) {
      return $http.post(apiUrl, user)
        .then(function (resp) {
          return resp.data
        })
    }

  })